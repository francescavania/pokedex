# Pokedex
https://pokedex-fravania.netlify.app/

Catch pokemon with 50% success probability. 
User can give the Pokemon a nickname and add that Pokemon to `MyPokemon List’.
The pokemons in the list persist even after a full page reload.

### Built With
* React JS
* React Hooks
* GraphQL
* Apollo Client
* Reactive Variable
* CSS in JS Emotion

## Installation

1. Clone the repo

2. Install Yarn packages
   ```sh
   yarn install
   ```